﻿using EGIC.Core.Models.APIs;
using EGIC.Web.Framework.Mvc.ModelBinding;
using System;
using System.ComponentModel.DataAnnotations;
using System.Data;

namespace EGIC.Core.Domain.APIs
{
    public partial class API : BaseEGICEntity
    {
        #region CTOR

        public API() : base()
        {
        }

        public API(APIModel model) : base()
        {
            this.Code = model.Code;
            this.Name = model.Name;
            this.ServerCode = model.ServerCode;
            this.DatabaseCode = model.DatabaseCode;
            this.Environment = model.Environment;
            this.Responsible = model.Responsible;
            this.Technology = model.Technology;
            this.Url = model.Url;
            this.DeploymentPath = model.DeploymentPath;
            this.SourceControlRepo = model.SourceControlRepo;
            this.CreatedOn = model.CreatedOn;
        }

        public API(DataRow row) : base(row)
        {
            try
            {
                Name = row[NAME].ToString();
                Technology = row[TECHNOLOGY].ToString();
                Url = row[URL].ToString();
                Environment = row[ENVIRONMENT].ToString();
                Responsible = row[RESPONSIBLE].ToString();
                DeploymentPath = row[DEPLOYMENT_PATH].ToString();
                SourceControlRepo = row[SOURCE_CONTROL_REPO].ToString();

                if (int.TryParse(row[SERVER_CODE].ToString(), out int _serverCode))
                    ServerCode = _serverCode;

                if (int.TryParse(row[DATABASE_CODE].ToString(), out int _databaseCode))
                    DatabaseCode = _databaseCode;

                if (DateTime.TryParse(row[CREATED_ON].ToString(), out DateTime _createdOn))
                    CreatedOn = _createdOn;

            }
            catch { }
        }

        #endregion

        #region Properties

        [EGICResourceDisplayName("Name")]
        public string Name { get; set; }

        [EGICResourceDisplayName("Server")]
        public int ServerCode { get; set; }

        [EGICResourceDisplayName("Database")]
        public int DatabaseCode { get; set; }

        [EGICResourceDisplayName("Technology")]
        public string Technology { get; set; }

        [EGICResourceDisplayName("Url")]
        public string Url { get; set; }

        [EGICResourceDisplayName("Environment")]
        public string Environment { get; set; }

        [EGICResourceDisplayName("Responsible")]
        public string Responsible { get; set; }

        [EGICResourceDisplayName("Deployment Path")]
        public string DeploymentPath { get; set; }

        [EGICResourceDisplayName("Source Control Repo")]
        public string SourceControlRepo { get; set; }

        [EGICResourceDisplayName("Created On")]
        public DateTime CreatedOn { get; set; }

        #endregion
    }
}
