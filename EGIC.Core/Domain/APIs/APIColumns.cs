﻿namespace EGIC.Core.Domain.APIs
{
    public partial class API
    {
        public const string NAME = nameof(NAME);
        public const string SERVER_CODE = nameof(SERVER_CODE);
        public const string DATABASE_CODE = nameof(DATABASE_CODE);
        public const string TECHNOLOGY = nameof(TECHNOLOGY);
        public const string URL = nameof(URL);
        public const string ENVIRONMENT = nameof(ENVIRONMENT);
        public const string RESPONSIBLE = nameof(RESPONSIBLE);
        public const string DEPLOYMENT_PATH = nameof(DEPLOYMENT_PATH);
        public const string SOURCE_CONTROL_REPO = nameof(SOURCE_CONTROL_REPO);
        public const string CREATED_ON = nameof(CREATED_ON);
    }
}
