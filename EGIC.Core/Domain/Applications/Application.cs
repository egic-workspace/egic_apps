﻿using EGIC.Core.Models.Applications;
using EGIC.Web.Framework.Mvc.ModelBinding;
using System;
using System.Data;

namespace EGIC.Core.Domain.Applications
{
    public partial class Application : BaseEGICEntity
    {
        #region CTOR

        public Application() : base()
        {
        }

        public Application(ApplicationModel model) : base()
        {
            this.Code = model.Code;
            this.Name = model.Name;
            this.APICode = model.APICode;
            this.DatabaseCode = model.DatabaseCode;
            this.PlatformType = model.PlatformType;
            this.HostServerCode = model.HostServerCode;
            this.AdminUsername = model.AdminUsername;
            this.AdminPassword = model.AdminPassword;
            this.Responsible = model.Responsible;
            this.Url = model.Url;
            this.DeploymentPath = model.DeploymentPath;
            this.SourceControlRepo = model.SourceControlRepo;
            this.CreatedOn = model.CreatedOn;
        }

        public Application(DataRow row) : base(row)
        {
            try
            {
                Name = row[NAME].ToString();
                AdminUsername = row[ADMIN_USERNAME].ToString();
                AdminPassword = row[ADMIN_PASSWORD].ToString();
                PlatformType = row[PLATFORM_TYPE].ToString();
                Responsible = row[RESPONSIBLE].ToString();
                Url = row[URL].ToString();
                DeploymentPath = row[DEPLOYMENT_PATH].ToString();
                SourceControlRepo = row[SOURCE_CONTROL_REPO].ToString();

                if (int.TryParse(row[API_CODE].ToString(), out int apiCode) && apiCode > 0)
                    APICode = apiCode;
                
                if (int.TryParse(row[DATABASE_CODE].ToString(), out int datebaseCode) && datebaseCode > 0)
                    DatabaseCode = datebaseCode;
                
                if (int.TryParse(row[HOST_SERVER_CODE].ToString(), out int serverCode) && serverCode > 0)
                    HostServerCode = serverCode;

                if (DateTime.TryParse(row[CREATED_ON].ToString(), out DateTime _createdOn))
                    CreatedOn = _createdOn;


            }
            catch { }
        }

        #endregion

        #region Properties

        [EGICResourceDisplayName("Name")]
        public string Name { get; set; }

        [EGICResourceDisplayName("API")]
        public int? APICode { get; set; }

        [EGICResourceDisplayName("Database")]
        public int? DatabaseCode { get; set; }

        [EGICResourceDisplayName("Admin Username")]
        public string AdminUsername { get; set; }

        [EGICResourceDisplayName("Admin Password")]
        public string AdminPassword { get; set; }

        [EGICResourceDisplayName("Platform Type")]
        public string PlatformType { get; set; }

        [EGICResourceDisplayName("Host Server")]
        public int? HostServerCode { get; set; }

        [EGICResourceDisplayName("Responsible")]
        public string Responsible { get; set; }

        [EGICResourceDisplayName("Url")]
        public string Url { get; set; }

        [EGICResourceDisplayName("Deployment Path")]
        public string DeploymentPath { get; set; }

        [EGICResourceDisplayName("Source Control Repo")]
        public string SourceControlRepo { get; set; }

        [EGICResourceDisplayName("Created On")]
        public DateTime CreatedOn { get; set; }


        #endregion
    }
}
