﻿namespace EGIC.Core.Domain.Applications
{
    public partial class Application
    {
        public const string NAME = nameof(NAME);
        public const string API_CODE = nameof(API_CODE);
        public const string DATABASE_CODE = nameof(DATABASE_CODE);
        public const string ADMIN_USERNAME = nameof(ADMIN_USERNAME);
        public const string ADMIN_PASSWORD = nameof(ADMIN_PASSWORD);
        public const string PLATFORM_TYPE = nameof(PLATFORM_TYPE);
        public const string HOST_SERVER_CODE = nameof(HOST_SERVER_CODE);
        public const string RESPONSIBLE = nameof(RESPONSIBLE);
        public const string URL = nameof(URL);
        public const string DEPLOYMENT_PATH = nameof(DEPLOYMENT_PATH);
        public const string SOURCE_CONTROL_REPO = nameof(SOURCE_CONTROL_REPO);
        public const string CREATED_ON = nameof(CREATED_ON);
    }
}
