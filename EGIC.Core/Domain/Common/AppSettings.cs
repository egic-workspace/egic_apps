﻿using System.Data;

namespace EGIC.Core.Domain.Common
{
    public partial class AppSetting : BaseEGICEntity
    {
        public AppSetting()
        {
        }
        public AppSetting(DataRow row)
        {
            if (row != null)
            {
                SettingName = row[SETTING_NAME].ToString();
                SettingValue = row[SETTING_VALUE].ToString();
            }
        }

        public string SettingName { get; set; }
        public string SettingValue { get; set; }
    }
    public partial class AppSetting
    {
        const string SETTING_NAME = nameof(SETTING_NAME);
        const string SETTING_VALUE = nameof(SETTING_VALUE);
    } 
}