﻿using EGIC.Core.Models.Databases;
using EGIC.Web.Framework.Mvc.ModelBinding;
using System.Data;

namespace EGIC.Core.Domain.Databases
{
    public partial class Database : BaseEGICEntity
    {
        #region CTOR

        public Database() : base()
        {
        }

        public Database(DatabaseModel model) : base()
        {
            this.Code = model.Code;
            this.ServiceName = model.ServiceName;
            this.Environment = model.Environment;
            this.ServerCode = model.ServerCode;
            this.UserSchema = model.UserSchema;
            this.Password = model.Password;
        }

        public Database(DataRow row) : base(row)
        {
            try
            {
                ServiceName = row[SERVICE_NAME].ToString();
                UserSchema = row[USES_SCHEMA].ToString();
                Password = row[PASSWORD].ToString();
                Environment = row[ENVIRONMENT].ToString();

                if (int.TryParse(row[SERVER_CODE].ToString(), out int _serverCode))
                    ServerCode = _serverCode;
            }
            catch { }
        }

        #endregion

        #region Properties

        [EGICResourceDisplayName("Service Name")]
        public string ServiceName { get; set; }
         
        [EGICResourceDisplayName("User Schema")]
        public string UserSchema { get; set; }

        [EGICResourceDisplayName("Password")]
        public string Password { get; set; }
         
        [EGICResourceDisplayName("Environment")]
        public string Environment { get; set; }

        [EGICResourceDisplayName("Server")]
        public int ServerCode { get; set; }

        #endregion
    }
}
