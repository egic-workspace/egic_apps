﻿namespace EGIC.Core.Domain.Databases
{
    public partial class Database
    {
        public const string SERVICE_NAME = nameof(SERVICE_NAME);
        public const string USES_SCHEMA = nameof(USES_SCHEMA);
        public const string PASSWORD = nameof(PASSWORD);
        public const string ENVIRONMENT = nameof(ENVIRONMENT);
        public const string SERVER_CODE = nameof(SERVER_CODE);
    }
}
