﻿using EGIC.Core.Models.Servers;
using EGIC.Web.Framework.Mvc.ModelBinding;
using System.ComponentModel.DataAnnotations;
using System.Data;

namespace EGIC.Core.Domain.Servers
{
    public partial class Server : BaseEGICEntity
    {
        #region CTOR

        public Server() : base()
        {
        }
        
        public Server(ServerModel model) : base()
        {
            this.Code = model.Code;
            this.Name = model.Name;
            this.InternalIP = model.InternalIP;
            this.ExternalIP = model.ExternalIP;
            this.Location = model.Location;
            this.Scope = model.Scope;
            this.AdminUsername = model.AdminUsername;
            this.AdminPassword = model.AdminPassword;
            this.Deleted = model.Deleted;
        }

        public Server(DataRow row) : base(row) 
        {
            try
            {
                Name = row[NAME].ToString();
                AdminUsername = row[ADMIN_USERNAME].ToString();
                AdminPassword = row[ADMIN_PASSWORD].ToString();
                InternalIP = row[INTERNAL_IP].ToString();
                ExternalIP = row[EXTERNAL_IP].ToString();
                Location = row[LOCATION].ToString();
                Scope = row[SCOPE].ToString();
                Deleted = row[DELETED].ToString() == "1";
            }
            catch { }
        }

        #endregion

        #region Properties

        [EGICResourceDisplayName("Name")]
        public string Name { get; set; }

        [EGICResourceDisplayName("Internal IP")]
        [RegularExpression(@"\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\b",ErrorMessage = "Invalid IP address")]
        public string InternalIP { get; set; }

        [EGICResourceDisplayName("External IP")]
        [RegularExpression(@"\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\b", ErrorMessage = "Invalid IP address")]
        public string ExternalIP { get; set; }

        [EGICResourceDisplayName("Admin Username")]
        public string AdminUsername { get; set; }

        [EGICResourceDisplayName("Admin Password")]
        public string AdminPassword { get; set; }

        [EGICResourceDisplayName("Location")]
        public string Location { get; set; }

        [EGICResourceDisplayName("Scope")]
        public string Scope { get; set; }

        public bool Deleted { get; set; }

        #endregion
    }
}