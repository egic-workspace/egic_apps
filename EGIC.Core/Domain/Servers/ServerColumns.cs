﻿namespace EGIC.Core.Domain.Servers
{
    public partial class Server
    {
        public const string NAME = nameof(NAME);
        public const string INTERNAL_IP = nameof(INTERNAL_IP);
        public const string EXTERNAL_IP = nameof(EXTERNAL_IP);
        public const string ADMIN_USERNAME = nameof(ADMIN_USERNAME);
        public const string ADMIN_PASSWORD = nameof(ADMIN_PASSWORD);
        public const string LOCATION = nameof(LOCATION);
        public const string SCOPE = nameof(SCOPE);
        public const string DELETED = nameof(DELETED);
    }
}
 