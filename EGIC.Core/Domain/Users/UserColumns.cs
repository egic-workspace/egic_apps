﻿namespace EGIC.Core.Domain.Users
{
    public partial class User
    {
        public const string USER_NAME = nameof(USER_NAME);
        public const string FULL_NAME = nameof(FULL_NAME);
        public const string PASSWORD = nameof(PASSWORD);
    }
}