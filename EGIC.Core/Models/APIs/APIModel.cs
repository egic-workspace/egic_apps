﻿using EGIC.Core.Domain.APIs;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;

namespace EGIC.Core.Models.APIs
{
    public class APIModel : API
    {
        public APIModel()
        {
            Servers = new List<SelectListItem>();
            Databases = new List<SelectListItem>();
            Environments = new List<SelectListItem>();
        }
        public APIModel(API entity):this()
        {
            this.Code = entity.Code;
            this.Name = entity.Name;
            this.ServerCode = entity.ServerCode;
            this.DatabaseCode = entity.DatabaseCode;
            this.Environment = entity.Environment;
            this.Responsible = entity.Responsible;
            this.Technology = entity.Technology;
            this.Url = entity.Url;
            this.DeploymentPath = entity.DeploymentPath;
            this.SourceControlRepo = entity.SourceControlRepo;
            this.CreatedOn = entity.CreatedOn;
        }

        public List<SelectListItem> Servers { get; set; }
        public List<SelectListItem> Databases { get; set; }
        public List<SelectListItem> Environments { get; set; }
    }
}
