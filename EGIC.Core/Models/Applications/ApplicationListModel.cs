﻿using EGIC.Web.Framework.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;

namespace EGIC.Core.Models.Applications
{
    public partial class ApplicationListModel
    {
        public ApplicationListModel()
        {
            HostingServers = new List<SelectListItem>();
            PlatformTypes = new List<SelectListItem>();
            Responsibles = new List<SelectListItem>();
        }
        
        [EGICResourceDisplayName("Responsible")]
        public string Responsible { get; set; }
        public List<SelectListItem> Responsibles { get; set; }

        [EGICResourceDisplayName("Platform Type")]
        public string PlatformType { get; set; }
        public List<SelectListItem> PlatformTypes { get; set; }

        [EGICResourceDisplayName("Hosting Server")]
        public int? HostingServerCode { get; set; }
        public List<SelectListItem> HostingServers { get; set; }
    }
}