﻿using EGIC.Core.Domain.Applications;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;

namespace EGIC.Core.Models.Applications
{
    public class ApplicationModel : Application
    {
        public ApplicationModel()
        {
            Servers = new List<SelectListItem>();
            Databases = new List<SelectListItem>();
            APIs = new List<SelectListItem>();
        }
        public ApplicationModel(Application entity):this()
        {
            this.Code = entity.Code;
            this.Name = entity.Name;
            this.APICode = entity.APICode;
            this.DatabaseCode = entity.DatabaseCode;
            this.PlatformType = entity.PlatformType;
            this.HostServerCode = entity.HostServerCode;
            this.AdminUsername = entity.AdminUsername;
            this.AdminPassword = entity.AdminPassword;
            this.Responsible = entity.Responsible;
            this.Url = entity.Url;
            this.DeploymentPath = entity.DeploymentPath;
            this.SourceControlRepo = entity.SourceControlRepo;
            this.CreatedOn = entity.CreatedOn;
        }

        public List<SelectListItem> Servers { get; set; }
        public List<SelectListItem> Databases { get; set; }
        public List<SelectListItem> APIs { get; set; }
    }
}
