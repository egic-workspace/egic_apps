﻿using EGIC.Core.Domain.Databases;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;

namespace EGIC.Core.Models.Databases
{
    public class DatabaseModel : Database
    {
        public DatabaseModel()
        {
            Servers = new List<SelectListItem>();
            Environments = new List<SelectListItem>();
        }
        public DatabaseModel(Database entity):this()
        {
            this.Code = entity.Code;
            this.ServiceName = entity.ServiceName;
            this.UserSchema = entity.UserSchema;
            this.Password = entity.Password;
            this.Environment = entity.Environment;
            this.ServerCode = entity.ServerCode;
        }
        public List<SelectListItem> Servers { get; set; }
        public List<SelectListItem> Environments { get; set; }
    }
}
