﻿using System.ComponentModel.DataAnnotations;
using EGIC.Web.Framework.Mvc.ModelBinding;
using EGIC.Web.Framework.Mvc.Models;

namespace EGIC.Core.Models.Security
{
    public partial class LoginModel 
    {
        [EGICResourceDisplayName("Username")]
        [Required(ErrorMessage ="*")]
        public string Username { get; set; }

        [DataType(DataType.Password)]
        [EGICResourceDisplayName("Password")]
        [Required(ErrorMessage = "*")]
        public string Password { get; set; }
    }
}