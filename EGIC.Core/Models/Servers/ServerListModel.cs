﻿using EGIC.Web.Framework.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;

namespace EGIC.Core.Models.Servers
{
    public partial class ServerListModel
    {
        public ServerListModel()
        {
            ServerLocations = new List<SelectListItem>();
        }

        [EGICResourceDisplayName("Internal IP")]
        public string SearchServerInternalIP { get; set; }

        [EGICResourceDisplayName("External IP")]
        public string SearchServerExternalIP { get; set; }

        [EGICResourceDisplayName("Server Name")]
        public string SearchServerName { get; set; }

        [EGICResourceDisplayName("Server Location")]
        public string SearchServerLocation { get; set; }
        public List<SelectListItem> ServerLocations { get; set; }
    }
}