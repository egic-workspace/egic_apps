﻿using EGIC.Core.Domain.Servers;
using System;
using System.Collections.Generic;
using System.Text;

namespace EGIC.Core.Models.Servers
{
    public class ServerModel : Server
    {
        public ServerModel()
        {

        }
        public ServerModel(Server obj)
        {
            this.Code = obj.Code;
            this.Name = obj.Name;
            this.InternalIP = obj.InternalIP;
            this.ExternalIP = obj.ExternalIP;
            this.Location = obj.Location;
            this.Scope= obj.Scope;
            this.AdminUsername= obj.AdminUsername;
            this.AdminPassword= obj.AdminPassword;
            this.Deleted= obj.Deleted;
        }
    }
}
