﻿using EGIC.Web.Framework.Mvc.Models;

namespace EGIC.Core.Models.Settings
{
    public partial class ModeModel 
    {
        public string ModeName { get; set; }
        public bool Enabled { get; set; }
    }
}