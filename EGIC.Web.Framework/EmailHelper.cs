﻿using System;
using System.Net.Mail;

namespace EGIC.Web.Framework
{
    public static class EmailHelper
    {
        public static void SendMail(string mailFrom, string mailsTo, string ccMails, string subject, string messageBody)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(mailsTo))
                    return;

                MailMessage message = new MailMessage();
                mailFrom = mailFrom ?? "EGICApps_sys@egic.com.eg";
                string fromName = mailFrom.Substring(0, mailFrom.IndexOf("@"));
                fromName = fromName.Replace('.', ' ');

                message.From = new MailAddress(mailFrom, fromName);
                message.To.Add(mailsTo);

                if (!string.IsNullOrWhiteSpace(ccMails))
                    message.CC.Add(ccMails);

                message.Bcc.Add("saad.bakr@egic.com.eg");
                message.Subject = subject;
                message.Body = messageBody;
                message.IsBodyHtml = true;

                SmtpClient smtpClient = new SmtpClient();
                smtpClient.Host = "Mail2.egic.com.eg";

                smtpClient.Send(message);
            }
            catch (Exception ex)
            {
                string error = ex.Message;
            }
        }
    }
}