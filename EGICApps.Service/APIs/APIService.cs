﻿using EGIC.Core.Domain.APIs;
using EGIC.Data.DAL;
using EGIC.Web.Framework;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;

namespace EGICApps.Services.APIs
{
    public class APIService : IAPIService
    {
        #region Fields

        private readonly IOracleAcess _oracleAcess;
        private readonly IHostingEnvironment _hostingEnvironment;

        #endregion

        #region CTOR

        public APIService(IServiceProvider service, IHostingEnvironment hostingEnvironment)
        {
            _oracleAcess = service.GetService<IOracleAcess>();
            _hostingEnvironment = hostingEnvironment;
        }

        #endregion

        #region Methods

        public virtual IPagedList<API> GetAllAPIs(int serverCode = 0, int pageIndex = 0, int pageSize = 20)
        {
            var entities = new List<API>();
            string sqlQuery =
                $@"SELECT 
                        {API.CODE},
                        {API.NAME},
                        {API.SERVER_CODE},
                        {API.DATABASE_CODE},
                        {API.TECHNOLOGY},
                        {API.URL},
                        {API.SOURCE_CONTROL_REPO},
                        {API.DEPLOYMENT_PATH},
                        {API.ENVIRONMENT},
                        {API.RESPONSIBLE},
                        {API.CREATED_ON}
                    FROM 
                        EGIT_API C
                    WHERE 
                        {API.CODE} > 0 
                        {(serverCode > 0 ? $" AND {API.SERVER_CODE} = {serverCode} " : "")}
                    ORDER BY  
                        {API.CODE} DESC ";

            var data = _oracleAcess.ExecuteDataSet(sqlQuery);
            for (int i = 0; i < data.Tables[0].Rows.Count; i++)
                entities.Add(new API(data.Tables[0].Rows[i]));

            return new PagedList<API>(entities, pageIndex - 1, pageSize);
        }

        public API GetAPIByCode(int entityCode)
        {
            string sqlQuery =
                $@"SELECT 
                        {API.CODE},
                        {API.NAME},
                        {API.SERVER_CODE},
                        {API.DATABASE_CODE},
                        {API.TECHNOLOGY},
                        {API.URL},
                        {API.SOURCE_CONTROL_REPO},
                        {API.DEPLOYMENT_PATH},
                        {API.ENVIRONMENT},
                        {API.RESPONSIBLE},
                        {API.CREATED_ON}
                    FROM 
                        EGIT_API C
                    WHERE 
                        {API.CODE} = {entityCode}";

            var data = _oracleAcess.ExecuteDataSet(sqlQuery);
            if (data.Tables[0].Rows.Count == 0)
                return null;

            return new API(data.Tables[0].Rows[0]);
        }

        public bool AddAPI(API entity)
        {
            if (entity == null)
                return false;

            string sqlCommand = 
                $@"INSERT INTO EGIT_API 
                    (
                        {API.NAME},
                        {API.SERVER_CODE},
                        {API.DATABASE_CODE},
                        {API.TECHNOLOGY},
                        {API.URL},
                        {API.SOURCE_CONTROL_REPO},
                        {API.DEPLOYMENT_PATH},
                        {API.ENVIRONMENT},
                        {API.RESPONSIBLE}
                    ) 
                   VALUES 
                    (
                        '{entity.Name}',
                        '{entity.ServerCode}',
                        '{entity.DatabaseCode}',
                        '{entity.Technology}',
                        '{entity.Url}',
                        '{entity.SourceControlRepo}',
                        '{entity.DeploymentPath}',
                        '{entity.Environment}',
                        '{entity.Responsible}'
                    )";
            var effictedRows = _oracleAcess.ExecuteNonQuery(sqlCommand);
            return effictedRows > 0;
        }

        public bool DeleteAPI(int code)
        {
            if (code < 1)
                return false;

            string sqlCommand =
                $"DELETE FROM EGIT_API WHERE {API.CODE} = {code}";

            var effictedRows = _oracleAcess.ExecuteNonQuery(sqlCommand);
            return effictedRows > 0;
        }

        public bool UpdateAPI(API entity)
        {
            string sqlCommand = 
                $@"UPDATE EGIT_API 
                        SET {API.NAME} = '{entity.Name}',
                        {API.SERVER_CODE} = '{entity.ServerCode}',
                        {API.DATABASE_CODE}= '{entity.DatabaseCode}',
                        {API.TECHNOLOGY}= '{entity.Technology}',
                        {API.URL}= '{entity.Url}',
                        {API.SOURCE_CONTROL_REPO}= '{entity.SourceControlRepo}',
                        {API.DEPLOYMENT_PATH}= '{entity.DeploymentPath}',
                        {API.ENVIRONMENT}= '{entity.Environment}',
                        {API.RESPONSIBLE}= '{entity.Responsible}'
                    WHERE {API.CODE}  = {entity.Code}
                ";
            var effictedRows = _oracleAcess.ExecuteNonQuery(sqlCommand);
            return effictedRows > 0;
        }

        #endregion
    }
}