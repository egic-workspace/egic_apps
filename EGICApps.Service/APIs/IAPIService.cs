﻿using EGIC.Core.Domain.APIs;
using EGIC.Web.Framework;
using System;
using System.Collections.Generic;

namespace EGICApps.Services.APIs
{
    public interface IAPIService
    {
        IPagedList<API> GetAllAPIs(int serverCode = 0, int pageIndex = 0, int pageSize = 20);

        API GetAPIByCode(int apiCode);
        
        bool AddAPI(API entity);

        bool UpdateAPI(API entity);

        bool DeleteAPI(int code);

    }
}