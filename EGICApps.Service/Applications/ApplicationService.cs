﻿using EGIC.Core.Domain.Applications;
using EGIC.Data.DAL;
using EGIC.Web.Framework;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;

namespace EGICApps.Services.Applications
{
    public class ApplicationService : IApplicationService
    {
        #region Fields

        private readonly IOracleAcess _oracleAcess;

        #endregion

        #region CTOR

        public ApplicationService(IServiceProvider service)
        {
            _oracleAcess = service.GetService<IOracleAcess>();
        }

        #endregion

        #region Methods

        public virtual IPagedList<Application> GetAllApplications(int serverCode = 0, string responsible = null, string platformType = null, int pageIndex = 0, int pageSize = 20)
        {
            var entities = new List<Application>();
            string sqlQuery =
                $@"SELECT 
                        {Application.CODE},
                        {Application.NAME},
                        {Application.API_CODE},
                        {Application.DATABASE_CODE},
                        {Application.ADMIN_USERNAME},
                        {Application.ADMIN_PASSWORD},
                        {Application.PLATFORM_TYPE},
                        {Application.HOST_SERVER_CODE},
                        {Application.RESPONSIBLE},
                        {Application.URL},
                        {Application.DEPLOYMENT_PATH},
                        {Application.SOURCE_CONTROL_REPO},
                        {Application.CREATED_ON}
                    FROM 
                        EGIT_APP C
                    WHERE 
                        {Application.CODE} > 0 
                        {(serverCode > 0 ? $" AND {Application.HOST_SERVER_CODE} = {serverCode} " : "")}
                        {(!string.IsNullOrWhiteSpace(responsible) ? $" AND {Application.RESPONSIBLE} = '{responsible}' " : "")}
                        {(!string.IsNullOrWhiteSpace(platformType) ? $" AND {Application.PLATFORM_TYPE} = '{platformType}' " : "")}
                    ORDER BY  
                        {Application.CODE} DESC ";

            var data = _oracleAcess.ExecuteDataSet(sqlQuery);
            for (int i = 0; i < data.Tables[0].Rows.Count; i++)
                entities.Add(new Application(data.Tables[0].Rows[i]));

            return new PagedList<Application>(entities, pageIndex - 1, pageSize);
        }

        public Application GetApplicationByCode(int entityCode)
        {
            string sqlQuery =
                $@"SELECT 
                        {Application.CODE},
                        {Application.NAME},
                        {Application.API_CODE},
                        {Application.DATABASE_CODE},
                        {Application.ADMIN_USERNAME},
                        {Application.ADMIN_PASSWORD},
                        {Application.PLATFORM_TYPE},
                        {Application.HOST_SERVER_CODE},
                        {Application.RESPONSIBLE},
                        {Application.URL},
                        {Application.DEPLOYMENT_PATH},
                        {Application.SOURCE_CONTROL_REPO},
                        {Application.CREATED_ON}
                    FROM 
                        EGIT_APP C
                    WHERE 
                        {Application.CODE} = {entityCode}";

            var data = _oracleAcess.ExecuteDataSet(sqlQuery);
            if (data.Tables[0].Rows.Count == 0)
                return null;

            return new Application(data.Tables[0].Rows[0]);
        }

        public bool AddApplication(Application entity)
        {
            if (entity == null)
                return false;

            string sqlCommand =
                $@"INSERT INTO EGIT_APP 
                    (
                        {Application.NAME},
                        {Application.API_CODE},
                        {Application.DATABASE_CODE},
                        {Application.ADMIN_USERNAME},
                        {Application.ADMIN_PASSWORD},
                        {Application.PLATFORM_TYPE},
                        {Application.HOST_SERVER_CODE},
                        {Application.RESPONSIBLE},
                        {Application.URL},
                        {Application.DEPLOYMENT_PATH},
                        {Application.SOURCE_CONTROL_REPO}
                    ) 
                   VALUES 
                    (
                        '{entity.Name}',
                        '{entity.APICode}',
                        '{entity.DatabaseCode}',
                        '{entity.AdminUsername}',
                        '{entity.AdminPassword}',
                        '{entity.PlatformType}',
                        '{entity.HostServerCode}',
                        '{entity.Responsible}',
                        '{entity.Url}',
                        '{entity.DeploymentPath}',
                        '{entity.SourceControlRepo}'
                    )";
            var effictedRows = _oracleAcess.ExecuteNonQuery(sqlCommand);
            return effictedRows > 0;
        }

        public bool DeleteApplication(int code)
        {
            if (code < 1)
                return false;

            string sqlCommand =
                $"DELETE FROM EGIT_APP WHERE {Application.CODE} = {code}";

            var effictedRows = _oracleAcess.ExecuteNonQuery(sqlCommand);
            return effictedRows > 0;
        }

        public bool UpdateApplication(Application entity)
        {
            string sqlCommand =
                $@"UPDATE EGIT_APP 
                        SET {Application.NAME} = '{entity.Name}',
                        {Application.API_CODE} = '{entity.APICode}',
                        {Application.DATABASE_CODE}= '{entity.DatabaseCode}',
                        {Application.ADMIN_USERNAME}= '{entity.AdminUsername}',
                        {Application.ADMIN_PASSWORD}= '{entity.AdminPassword}',
                        {Application.PLATFORM_TYPE}= '{entity.PlatformType}',
                        {Application.HOST_SERVER_CODE}= '{entity.HostServerCode}',
                        {Application.RESPONSIBLE}= '{entity.Responsible}',
                        {Application.URL}= '{entity.Url}',
                        {Application.DEPLOYMENT_PATH}= '{entity.DeploymentPath}',
                        {Application.SOURCE_CONTROL_REPO}= '{entity.SourceControlRepo}'
                    WHERE {Application.CODE}  = {entity.Code}
                ";
            var effictedRows = _oracleAcess.ExecuteNonQuery(sqlCommand);
            return effictedRows > 0;
        }

        #endregion
    }
}