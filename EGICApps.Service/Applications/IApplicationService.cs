﻿using EGIC.Core.Domain.Applications;
using EGIC.Web.Framework;
using System;
using System.Collections.Generic;

namespace EGICApps.Services.Applications
{
    public interface IApplicationService
    {
        IPagedList<Application> GetAllApplications(int serverCode = 0, string responsible = null, string platformType = null, int pageIndex = 0, int pageSize = 20);

        Application GetApplicationByCode(int apiCode);
        
        bool AddApplication(Application entity);

        bool UpdateApplication(Application entity);

        bool DeleteApplication(int code);

    }
}