using EGIC.Core.Models.Users;
using EGICApps.Services.Users;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using Microsoft.Extensions.DependencyInjection;
using System.Security.Claims;
using EGIC.Core.Domain.Users;
using System.Linq;

namespace EGICApps.Services.Authentication
{
    /// <summary>
    /// Represents service using cookie middle-ware for the authentication
    /// </summary>
    public partial class CookieAuthenticationService : IAuthenticationService
    {
        #region Fields

        private readonly IUserService _userService;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private User _cachedUser;

        #endregion

        #region Ctor

        /// <summary>
        /// Ctor
        /// </summary>
        /// <param name="service">Service Provider</param>
        /// <param name="httpContextAccessor">HTTP context accessors</param>
        public CookieAuthenticationService(IServiceProvider service, IHttpContextAccessor httpContextAccessor)
        {
            this._userService = service.GetService<IUserService>();
            this._httpContextAccessor = httpContextAccessor;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Sign in
        /// </summary>
        /// <param name="user">User</param>
        public virtual async void SignIn(User user)
        {
            if (user == null)
                throw new ArgumentNullException(nameof(user));

            //create claims for user's username and email
            var claims = new List<Claim>();

            if (!string.IsNullOrEmpty(user.Username))
                claims.Add(new Claim(ClaimTypes.Name, user.Username, ClaimValueTypes.String, EGICCookieAuthenticationDefaults.ClaimsIssuer));

            //create principal for the current authentication scheme
            var userIdentity = new ClaimsIdentity(claims, EGICCookieAuthenticationDefaults.AuthenticationScheme);
            var userPrincipal = new ClaimsPrincipal(userIdentity);

            //set value indicating whether session is persisted and the time at which the authentication was issued
            var authenticationProperties = new AuthenticationProperties
            {
                IssuedUtc = DateTime.UtcNow
            };

            //sign in
            await _httpContextAccessor.HttpContext.SignInAsync(EGICCookieAuthenticationDefaults.AuthenticationScheme, userPrincipal, authenticationProperties);

            //cache authenticated user
            _cachedUser = user;
        }

        /// <summary>
        /// Sign out
        /// </summary>
        public virtual async void SignOut()
        {
            //reset cached user
            _cachedUser = null;

            //and sign out from the current authentication scheme
            await _httpContextAccessor.HttpContext.SignOutAsync(EGICCookieAuthenticationDefaults.AuthenticationScheme);
        }

        /// <summary>
        /// Get authenticated user
        /// </summary>
        /// <returns>User</returns>
        public virtual User GetAuthenticatedUser()
        {
            //whether there is a cached user
            if (_cachedUser != null)
                return _cachedUser;

            //try to get authenticated user identity
            var authenticateResult = _httpContextAccessor.HttpContext.AuthenticateAsync(EGICCookieAuthenticationDefaults.AuthenticationScheme).Result;
            if (!authenticateResult.Succeeded)
                return null;

            User user = null;
            //try to get user by username
            var usernameClaim = authenticateResult.Principal.FindFirst(claim => claim.Type == ClaimTypes.Name
                && claim.Issuer.Equals(EGICCookieAuthenticationDefaults.ClaimsIssuer, StringComparison.InvariantCultureIgnoreCase));
            if (usernameClaim != null)
                user = _userService.GetUserByUsername(usernameClaim.Value);


            //whether the found user is available
            if (user == null)
                return null;

            //cache authenticated user
            _cachedUser = user;

            return _cachedUser;
        }

        /// <summary>
        /// Validate user
        /// </summary>
        /// <param name="usernameOrEmail">Username or email</param>
        /// <param name="password">Password</param>
        /// <param name="currentUser">get current user</param>
        /// <returns>Result</returns>
        public virtual UserLoginResults ValidateUser(string usernameOrEmail, string password, out User currentUser)
        {
            var user = _userService.GetUserByUsername(usernameOrEmail);
            currentUser = user;
            if (user == null)
                return UserLoginResults.UserNotExist;
            //W123YM45
            string pass = "";
            var skipList = new List<int>() { 0, 4, 5 };
            for (int i = 0; i < user.Password.Length; i++)
                if (!skipList.Contains(i))
                    pass += user.Password[i];
            if (!pass.Equals(password, StringComparison.InvariantCultureIgnoreCase))
                return UserLoginResults.WrongPassword;

            return UserLoginResults.Successful;
        }

        #endregion
    }
}