﻿using EGIC.Core.Domain.Common;
using EGIC.Data.DAL;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;

namespace EGICApps.Services.Common
{
    public class CommonService : ICommonService
    {
        #region Fields

        private readonly IOracleAcess _oracleAcess;

        #endregion

        #region CTOR

        public CommonService(IServiceProvider service)
        {
            _oracleAcess = service.GetService<IOracleAcess>();
        }

        #endregion

        #region Methods

        public virtual List<AppSetting> GetAppSettings()
        {
            string sql = "SELECT CODE,SETTING_NAME,SETTING_VALUE FROM APPS_SETTINGS WHERE APP_NAME = 'EGICApps'";
            var data = _oracleAcess.ExecuteDataSet(sql);

            if (data.Tables[0].Rows.Count == 0)
                return new List<AppSetting>();

            List<AppSetting> _appSettings = new List<AppSetting>();
            for (int i = 0; i < data.Tables[0].Rows.Count; i++)
                _appSettings.Add(new AppSetting(data.Tables[0].Rows[i]));

            return _appSettings;
        }

        public virtual List<SelectListItem> GetAllEnvironments(string selectedEnvironment = null)
        {
            return new List<SelectListItem>
            {
                new SelectListItem{ Text = "Select...",Value = "" , Selected = string.IsNullOrWhiteSpace(selectedEnvironment)},
                new SelectListItem{ Text = "Live",Value = "Live",Selected = selectedEnvironment == "Live"},
                new SelectListItem{ Text = "Test",Value = "Test",Selected = selectedEnvironment == "Test"}
            };
        }

        public virtual List<SelectListItem> GetAllAPIs(int? selectedAPI = null)
        {
            string sql = $"SELECT DISTINCT CODE as VALUE,NAME ||' - '|| ENVIRONMENT as TEXT FROM EGIT_API";
            var data = _oracleAcess.ExecuteDataSet(sql);

            List<SelectListItem> list = new List<SelectListItem> { new SelectListItem { Text = "Select...", Value = "" } };
            if (data.Tables[0].Rows.Count == 0)
                return list;

            string selectedItem = selectedAPI.HasValue ? selectedAPI.Value.ToString() : "";
            for (int i = 0; i < data.Tables[0].Rows.Count; i++)
            {
                var row = data.Tables[0].Rows[i];
                string selectValue = row["VALUE"].ToString();
                list.Add(new SelectListItem() { Text = row["TEXT"].ToString(), Value = selectValue, Selected = selectValue == selectedItem });
            }
            return list;
        }

        public virtual List<SelectListItem> GetAllDatabases(int? selectedDatabase = null)
        {
            string sql = $"SELECT DISTINCT CODE as VALUE,USES_SCHEMA ||' - '|| ENVIRONMENT as TEXT FROM EGIT_DATABASE";
            var data = _oracleAcess.ExecuteDataSet(sql);

            List<SelectListItem> list = new List<SelectListItem> { new SelectListItem { Text = "Select...", Value = "" } };
            if (data.Tables[0].Rows.Count == 0)
                return list;

            string selectedItem = selectedDatabase.HasValue ? selectedDatabase.Value.ToString() : "";
            for (int i = 0; i < data.Tables[0].Rows.Count; i++)
            {
                var row = data.Tables[0].Rows[i];
                string selectValue = row["VALUE"].ToString();
                list.Add(new SelectListItem() { Text = row["TEXT"].ToString(), Value = selectValue, Selected = selectValue == selectedItem });
            }
            return list;
        }

        public virtual List<SelectListItem> GetAllServers(int? selectedServer = null)
        {
            string sql = "SELECT DISTINCT CODE as VALUE,NAME ||' - '|| INTERNAL_IP as TEXT FROM EGIT_SERVER";
            var data = _oracleAcess.ExecuteDataSet(sql);

            List<SelectListItem> list = new List<SelectListItem> { new SelectListItem { Text = "Select...", Value = "" } };
            if (data.Tables[0].Rows.Count == 0)
                return list;

            string selectedItem = selectedServer.HasValue ? selectedServer.Value.ToString() : "";
            for (int i = 0; i < data.Tables[0].Rows.Count; i++)
            {
                var row = data.Tables[0].Rows[i];
                string selectValue = row["VALUE"].ToString();
                list.Add(new SelectListItem() { Text = row["TEXT"].ToString(), Value = selectValue, Selected = selectValue == selectedItem });
            }
            return list;
        }

        public virtual List<SelectListItem> GetAllResponsibles()
        {
            string sql = "SELECT DISTINCT RESPONSIBLE as VALUE,RESPONSIBLE as TEXT FROM EGIT_APP";
            var data = _oracleAcess.ExecuteDataSet(sql);

            List<SelectListItem> list = new List<SelectListItem> { new SelectListItem { Text = "Select...", Value = "" } };
            if (data.Tables[0].Rows.Count == 0)
                return list;

            for (int i = 0; i < data.Tables[0].Rows.Count; i++)
            {
                var row = data.Tables[0].Rows[i];
                string username = row["VALUE"].ToString();
                list.Add(new SelectListItem() { Text = row["TEXT"].ToString(), Value = username });
            }
            return list;
        }

        public virtual List<SelectListItem> GetAllPlatformTypes()
        {
            string sql = "SELECT DISTINCT PLATFORM_TYPE as VALUE,PLATFORM_TYPE as TEXT FROM EGIT_APP";
            var data = _oracleAcess.ExecuteDataSet(sql);

            List<SelectListItem> list = new List<SelectListItem> { new SelectListItem { Text = "Select...", Value = "" } };
            if (data.Tables[0].Rows.Count == 0)
                return list;

            for (int i = 0; i < data.Tables[0].Rows.Count; i++)
            {
                var row = data.Tables[0].Rows[i];
                string username = row["VALUE"].ToString();
                list.Add(new SelectListItem() { Text = row["TEXT"].ToString(), Value = username });
            }
            return list;
        }

        public virtual List<SelectListItem> GetAllLocations()
        {
            string sql = "SELECT DISTINCT LOCATION as VALUE,LOCATION as TEXT FROM EGIT_SERVER";
            var data = _oracleAcess.ExecuteDataSet(sql);

            List<SelectListItem> list = new List<SelectListItem> { new SelectListItem { Text = "Select...", Value = "" } };
            if (data.Tables[0].Rows.Count == 0)
                return list;

            for (int i = 0; i < data.Tables[0].Rows.Count; i++)
            {
                var row = data.Tables[0].Rows[i];
                string username = row["VALUE"].ToString();
                list.Add(new SelectListItem() { Text = row["TEXT"].ToString(), Value = username });
            }
            return list;
        }

        #endregion
    }
}