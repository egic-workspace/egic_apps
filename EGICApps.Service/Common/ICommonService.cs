﻿using EGIC.Core.Domain.Common;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;

namespace EGICApps.Services.Common
{
    public interface ICommonService
    {
        List<AppSetting> GetAppSettings();

        List<SelectListItem> GetAllLocations();

        List<SelectListItem> GetAllPlatformTypes();

        List<SelectListItem> GetAllResponsibles();

        List<SelectListItem> GetAllEnvironments(string selectedEnvironment = null);

        List<SelectListItem> GetAllAPIs(int? selectedAPI = null);

        List<SelectListItem> GetAllServers(int? selectedServer = null);

        List<SelectListItem> GetAllDatabases(int? selectedDatabase = null);
    }
}