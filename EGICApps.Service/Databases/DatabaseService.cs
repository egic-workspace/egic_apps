﻿using EGIC.Core.Domain.Databases;
using EGIC.Data.DAL;
using EGIC.Web.Framework;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;

namespace EGICApps.Services.Databases
{
    public class DatabaseService : IDatabaseService
    {
        #region Fields

        private readonly IOracleAcess _oracleAcess;
        private readonly IHostingEnvironment _hostingEnvironment;

        #endregion

        #region CTOR

        public DatabaseService(IServiceProvider service, IHostingEnvironment hostingEnvironment)
        {
            _oracleAcess = service.GetService<IOracleAcess>();
            _hostingEnvironment = hostingEnvironment;
        }

        #endregion

        #region Methods

        public virtual IPagedList<Database> GetAllDatabases( int serverCode = 0, int pageIndex = 0, int pageSize = 20)
        {
            var entities = new List<Database>();
            string sqlQuery =
                $@"SELECT 
                        {Database.CODE},
                        {Database.SERVICE_NAME},
                        {Database.USES_SCHEMA},
                        {Database.PASSWORD},
                        {Database.ENVIRONMENT},
                        {Database.SERVER_CODE}
                    FROM 
                        EGIT_Database C
                    WHERE 
                        {Database.CODE} > 0 
                        {(serverCode > 0 ? $" AND {Database.SERVER_CODE} = {serverCode} " : "")}
                    ORDER BY  
                        {Database.CODE} DESC ";

            var data = _oracleAcess.ExecuteDataSet(sqlQuery);
            for (int i = 0; i < data.Tables[0].Rows.Count; i++)
                entities.Add(new Database(data.Tables[0].Rows[i]));

            return new PagedList<Database>(entities, pageIndex - 1, pageSize);
        }

        public Database GetDatabaseByCode(int dbCode)
        {
            string sqlQuery =
                $@"SELECT 
                        {Database.CODE},
                        {Database.SERVICE_NAME},
                        {Database.USES_SCHEMA},
                        {Database.PASSWORD},
                        {Database.ENVIRONMENT},
                        {Database.SERVER_CODE}
                    FROM 
                        EGIT_Database C
                    WHERE 
                        {Database.CODE} = {dbCode}";

            var data = _oracleAcess.ExecuteDataSet(sqlQuery);
            if (data.Tables[0].Rows.Count == 0)
                return null;

            return new Database(data.Tables[0].Rows[0]);
        }

        public bool AddDatabase(Database db)
        {
            if (db == null)
                return false;

            string sqlCommand = 
                $@"INSERT INTO EGIT_Database 
                    (
                        {Database.SERVICE_NAME},
                        {Database.USES_SCHEMA},
                        {Database.PASSWORD},
                        {Database.ENVIRONMENT},
                        {Database.SERVER_CODE}
                    ) 
                   VALUES 
                    (
                        '{db.ServiceName}',
                        '{db.UserSchema}',
                        '{db.Password}',
                        '{db.Environment}',
                        '{db.ServerCode}'
                    )";
            var effictedRows = _oracleAcess.ExecuteNonQuery(sqlCommand);
            return effictedRows > 0;
        }

        public bool DeleteDatabase(int code)
        {
            if (code < 1)
                return false;

            string sqlCommand =
                $"DELETE FROM EGIT_Database WHERE {Database.CODE} = {code}";

            var effictedRows = _oracleAcess.ExecuteNonQuery(sqlCommand);
            return effictedRows > 0;
        }

        public bool UpdateDatabase(Database db)
        {
            string sqlCommand = 
                $@"UPDATE EGIT_Database 
                        SET {Database.SERVICE_NAME} = '{db.ServiceName}',
                        {Database.USES_SCHEMA} = '{db.UserSchema}',
                        {Database.PASSWORD}= '{db.Password}',
                        {Database.ENVIRONMENT}= '{db.Environment}',
                        {Database.SERVER_CODE}= '{db.ServerCode}'
                    WHERE {Database.CODE}  = {db.Code}
                ";
            var effictedRows = _oracleAcess.ExecuteNonQuery(sqlCommand);
            return effictedRows > 0;
        }

        #endregion
    }
}