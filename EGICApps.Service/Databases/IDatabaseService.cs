﻿using EGIC.Core.Domain.Databases;
using EGIC.Web.Framework;
using System;
using System.Collections.Generic;

namespace EGICApps.Services.Databases
{
    public interface IDatabaseService
    {
        IPagedList<Database> GetAllDatabases(int serverCode = 0, int pageIndex = 0, int pageSize = 20);

        Database GetDatabaseByCode(int serverCode);
        
        bool AddDatabase(Database db);

        bool UpdateDatabase(Database db);

        bool DeleteDatabase(int code);

    }
}