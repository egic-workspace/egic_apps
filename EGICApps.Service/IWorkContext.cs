﻿using EGIC.Core.Domain.Common;
using EGIC.Core.Domain.Users;
using EGIC.Core.Models.Security;
using System.Collections.Generic;

namespace EGICApps.Services
{
    /// <summary>
    /// Represents work context
    /// </summary>
    public interface IWorkContext
    {
        /// <summary>
        /// Gets or sets the current user
        /// </summary>
        User CurrentUser { get; }

    }
}