using System.Collections.Generic;
using EGIC.Core.Domain.Users;
using EGIC.Core.Models.Users;
using EGIC.Core.Models.Security;

namespace EGICApps.Services.Security
{
    /// <summary>
    /// Permission service interface
    /// </summary>
    public partial interface IPermissionService
    {
        /// <summary>
        /// Authorize permission
        /// </summary>
        /// <returns>true - authorized; otherwise, false</returns>
        bool Authorize();

        /// <summary>
        /// Authorize permission
        /// </summary>
        /// <param name="user">User</param>
        /// <returns>true - authorized; otherwise, false</returns>
        bool Authorize(User user);

    }
}