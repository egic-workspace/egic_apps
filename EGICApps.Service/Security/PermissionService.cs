using EGIC.Core.Domain.Users;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace EGICApps.Services.Security
{
    /// <summary>
    /// Permission service
    /// </summary>
    public partial class PermissionService : IPermissionService
    {

        #region Fields

        private readonly IWorkContext _workContext;

        #endregion

        #region Ctor

        /// <summary>
        /// Ctor
        /// </summary>
        /// <param name="service">User service</param>
        public PermissionService(IServiceProvider service)
        {
            _workContext = service.GetService<IWorkContext>();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Authorize permission
        /// </summary>
        /// <param name="permissionRecordSystemName">Permission record system name</param>
        /// <returns>true - authorized; otherwise, false</returns>
        public virtual bool Authorize()
        {
            if (_workContext.CurrentUser == null)
                return false;
            return Authorize(_workContext.CurrentUser);
        }

        /// <summary>
        /// Authorize permission
        /// </summary>
        /// <param name="user">User</param>
        /// <returns>true - authorized; otherwise, false</returns>
        public virtual bool Authorize(User user)
        {

            if (!string.IsNullOrWhiteSpace(user.Username) && !string.IsNullOrWhiteSpace(user.Password))
                //yes, we have such permission
                return true;

            //no permission found
            return false;
        }

        #endregion
    }
}