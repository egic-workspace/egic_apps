﻿using EGIC.Core.Domain.Servers;
using EGIC.Web.Framework;
using System;
using System.Collections.Generic;

namespace EGICApps.Services.Servers
{
    public interface IServerService
    {
        IPagedList<Server> GetAllServers(string name = null, string location = null, string internalIP = null, string externalIP = null, int pageIndex = 0, int pageSize = 20);
        
        Server GetServerByCode(int serverCode);
        
        bool AddServer(Server server);

        bool UpdateServer(Server server);

        bool DeleteServer(int code,bool permanentDelete = false);

    }
}