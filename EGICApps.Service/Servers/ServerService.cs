﻿using EGIC.Core.Domain.Servers;
using EGIC.Data.DAL;
using EGIC.Web.Framework;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;

namespace EGICApps.Services.Servers
{
    public class ServerService : IServerService
    {
        #region Fields

        private readonly IOracleAcess _oracleAcess;
        private readonly IHostingEnvironment _hostingEnvironment;

        #endregion

        #region CTOR

        public ServerService(IServiceProvider service, IHostingEnvironment hostingEnvironment)
        {
            _oracleAcess = service.GetService<IOracleAcess>();
            _hostingEnvironment = hostingEnvironment;
        }

        #endregion

        #region Methods

        public virtual IPagedList<Server> GetAllServers(string name = null, string location = null, string internalIP = null, string externalIP = null, int pageIndex = 0, int pageSize = 20)
        {
            var entities = new List<Server>();
            string sqlQuery =
                $@"SELECT 
                        {Server.CODE},
                        {Server.NAME},
                        {Server.INTERNAL_IP},
                        {Server.EXTERNAL_IP},
                        {Server.ADMIN_USERNAME},
                        {Server.ADMIN_PASSWORD},
                        {Server.LOCATION},
                        {Server.SCOPE},
                        {Server.DELETED}
                    FROM 
                        EGIT_SERVER C
                    WHERE 
                        ({Server.DELETED} = 0 OR {Server.DELETED} IS NULL) ";

            if (!string.IsNullOrWhiteSpace(name))
                sqlQuery += $" AND UPPER({Server.NAME}) LIKE UPPER('%{name}%') ";

            if (!string.IsNullOrWhiteSpace(location))
                sqlQuery += $" AND UPPER({Server.LOCATION}) = UPPER('{location}') ";

            if (!string.IsNullOrWhiteSpace(internalIP))
                sqlQuery += $" AND {Server.INTERNAL_IP} = '{internalIP}' ";

            if (!string.IsNullOrWhiteSpace(externalIP))
                sqlQuery += $" AND {Server.EXTERNAL_IP} = '{externalIP}' ";

            sqlQuery += $" ORDER BY { Server.CODE} DESC ";
            var data = _oracleAcess.ExecuteDataSet(sqlQuery);
            for (int i = 0; i < data.Tables[0].Rows.Count; i++)
                entities.Add(new Server(data.Tables[0].Rows[i]));

            return new PagedList<Server>(entities, pageIndex - 1, pageSize);
        }

        public Server GetServerByCode(int serverCode)
        {
            string sqlQuery =
                $@"SELECT 
                        {Server.CODE},
                        {Server.NAME},
                        {Server.INTERNAL_IP},
                        {Server.EXTERNAL_IP},
                        {Server.ADMIN_USERNAME},
                        {Server.ADMIN_PASSWORD},
                        {Server.LOCATION},
                        {Server.SCOPE},
                        {Server.DELETED}
                    FROM 
                        EGIT_SERVER C
                    WHERE 
                        ({Server.DELETED} = 0 OR {Server.DELETED} is NULL)
                        AND {Server.CODE} = {serverCode}";

            var data = _oracleAcess.ExecuteDataSet(sqlQuery);
            if (data.Tables[0].Rows.Count == 0)
                return null;

            return new Server(data.Tables[0].Rows[0]);
        }

        public bool AddServer(Server server)
        {
            if (server == null)
                return false;

            string sqlCommand = 
                $@"INSERT INTO EGIT_SERVER 
                    (
                        {Server.NAME},
                        {Server.INTERNAL_IP},
                        {Server.EXTERNAL_IP},
                        {Server.ADMIN_USERNAME},
                        {Server.ADMIN_PASSWORD},
                        {Server.LOCATION},
                        {Server.SCOPE},
                        {Server.DELETED}
                    ) 
                   VALUES 
                    (
                        '{server.Name}',
                        '{server.InternalIP}',
                        '{server.ExternalIP}',
                        '{server.AdminUsername}',
                        '{server.AdminPassword}',
                        '{server.Location}',
                        '{server.Scope}',
                        0
                    )";
            var effictedRows = _oracleAcess.ExecuteNonQuery(sqlCommand);
            return effictedRows > 0;
        }

        public bool DeleteServer(int code, bool permanentDelete = false)
        {
            if (code < 1)
                return false;

            string sqlCommand =
                permanentDelete ?
                $"DELETE FROM EGIT_SERVER WHERE {Server.CODE} = {code}" :
                $"UPDATE EGIT_SERVER SET {Server.DELETED} = 1 WHERE {Server.CODE} = {code}";

            var effictedRows = _oracleAcess.ExecuteNonQuery(sqlCommand);
            return effictedRows > 0;
        }

        public bool UpdateServer(Server server)
        {
            string sqlCommand = 
                $@"UPDATE EGIT_SERVER 
                        SET {Server.NAME} = '{server.Name}',
                        {Server.INTERNAL_IP} = '{server.InternalIP}',
                        {Server.EXTERNAL_IP} = '{server.ExternalIP}',
                        {Server.ADMIN_USERNAME} = '{server.AdminUsername}',
                        {Server.ADMIN_PASSWORD} = '{server.AdminPassword}',
                        {Server.LOCATION} = '{server.Location}',
                        {Server.SCOPE} = '{server.Scope}'
                    WHERE {Server.CODE} = '{server.Code}'
                ";
            var effictedRows = _oracleAcess.ExecuteNonQuery(sqlCommand);
            return effictedRows > 0;
        }

        #endregion
    }
}