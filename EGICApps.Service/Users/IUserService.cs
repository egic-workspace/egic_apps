﻿using EGIC.Core.Domain.Users;
using EGIC.Web.Framework;
using System;
using System.Collections.Generic;

namespace EGICApps.Services.Users
{
    public interface IUserService
    {

        /// <summary>
        /// Get user by username
        /// </summary>
        /// <param name="username">Username</param>
        /// <returns>User</returns>
        User GetUserByUsername(string username);
    }
}
