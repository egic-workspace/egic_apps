﻿using EGIC.Core.Domain.Users;
using EGIC.Data.DAL;
using EGIC.Web.Framework;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace EGICApps.Services.Users
{
    public class UserService : IUserService
    {
        #region Fields

        private readonly IOracleAcess _oracleAcess;

        #endregion

        #region CTOR

        public UserService(IServiceProvider service)
        {
            _oracleAcess = service.GetService<IOracleAcess>();
        }

        #endregion

        #region Methods


        
        /// <summary>
        /// Get user by username
        /// </summary>
        /// <param name="username">Username</param>
        /// <returns>User</returns>
        public virtual User GetUserByUsername(string username)
        {
            if (string.IsNullOrWhiteSpace(username))
                throw new ArgumentNullException(nameof(username));

            string sqlQuery = $@"Select  us.User_Name,us.pass PASSWORD, us.emp_no code, us.arabic_name FULL_NAME FROM users us where UPPER(us.User_Name) = '{username}'";// and us.pass='${mUser.PASSWORD }'
            var data = _oracleAcess.ExecuteDataSet(sqlQuery);
            if (data.Tables[0].Rows.Count == 0)
                return null;

            var entity = new User(data.Tables[0].Rows[0]);
            return entity;
        }

        #endregion
    }
}