﻿using EGIC.Core.Domain.Common;
using EGIC.Core.Domain.Users;
using EGIC.Data.DAL;
using EGICApps.Services.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EGICApps.Services
{
    /// <summary>
    /// Represents work context for web application
    /// </summary>
    public partial class WebWorkContext : IWorkContext
    {
        #region Const

        private const string CUSTOMER_COOKIE_NAME = ".EGIC.User";

        #endregion

        #region Fields

        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IAuthenticationService _authenticationService;
        private readonly IOracleAcess _oracleAcess;

        private User _cachedUser;

        #endregion

        #region Ctor

        /// <summary>
        /// Ctor
        /// </summary>
        /// <param name="httpContextAccessor">HTTP context accessors</param>
        /// <param name="service">service provider</param>
        public WebWorkContext(IHttpContextAccessor httpContextAccessor, IServiceProvider service)
        {
            this._httpContextAccessor = httpContextAccessor;
            this._authenticationService = service.GetService<IAuthenticationService>();
            this._oracleAcess = service.GetService<IOracleAcess>();

        }

        #endregion

        #region Utilities

        /// <summary>
        /// Get nop user cookie
        /// </summary>
        /// <returns>String value of cookie</returns>
        protected virtual string GetUserCookie()
        {
            return _httpContextAccessor.HttpContext?.Request?.Cookies[CUSTOMER_COOKIE_NAME];
        }

        /// <summary>
        /// Set nop user cookie
        /// </summary>
        /// <param name="userCode">Code of the user</param>
        protected virtual void SetUserCookie(int userCode)
        {
            if (_httpContextAccessor.HttpContext?.Response == null)
                return;

            //delete current cookie value
            _httpContextAccessor.HttpContext.Response.Cookies.Delete(CUSTOMER_COOKIE_NAME);

            //get date of cookie expiration
            var cookieExpires = 24 * 365; //TODO make configurable
            var cookieExpiresDate = DateTime.Now.AddHours(cookieExpires);

            //if passed code = 0 set cookie as expired
            if (userCode == 0)
                cookieExpiresDate = DateTime.Now.AddMonths(-1);

            //set new cookie value
            var options = new Microsoft.AspNetCore.Http.CookieOptions
            {
                HttpOnly = true,
                Expires = cookieExpiresDate
            };
            _httpContextAccessor.HttpContext.Response.Cookies.Append(CUSTOMER_COOKIE_NAME, userCode.ToString(), options);
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the current user
        /// </summary>
        public virtual User CurrentUser
        {
            get
            {
                //whether there is a cached value
                if (_cachedUser != null)
                    return _cachedUser;

                User user = _authenticationService.GetAuthenticatedUser();

                if (user != null )
                    _cachedUser = user;
                else
                    _cachedUser = null;

                return _cachedUser;
            }
            set
            {
                _cachedUser = value;
            }
        }


        #endregion
    }
}