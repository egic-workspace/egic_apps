﻿using EGIC.Core.Domain.APIs;
using EGIC.Core.Models.APIs;
using EGIC.Web.Framework.Kendoui;
using EGICApps.Services.APIs;
using EGICApps.Services.Common;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;

namespace EGICApps.Web.Controllers
{
    [Route("API")]
    public class APIController : BaseController
    {

        #region Fields

        private readonly IAPIService _applicationService;
        private readonly ICommonService _commonService;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly IConfiguration _config;

        #endregion

        #region Ctor

        public APIController(IServiceProvider service,
            IAPIService serverService,
            ICommonService commonService,
            IHostingEnvironment hostingEnvironment,
            IConfiguration config) : base(service)
        {
            _hostingEnvironment = hostingEnvironment;
            _applicationService = serverService;
            _commonService = commonService;
            _config = config;
        }

        #endregion

        #region Utilities

        private void PrepareModel(ref APIModel model, API entity)
        {
            int? selectedServer = null;
            int? selectedDatabase = null;
            string selectedEnvironment = null;
            if (entity != null)
            {
                selectedServer = entity.ServerCode;
                selectedDatabase = entity.DatabaseCode;
                selectedEnvironment = entity.Environment;
            }

            model.Servers = _commonService.GetAllServers(selectedServer);
            model.Databases = _commonService.GetAllDatabases(selectedDatabase);
            model.Environments = _commonService.GetAllEnvironments(selectedEnvironment);
        }


        private string ValidateModel(APIModel model)
        {
            if (model == null)
                return $"{nameof(model)} can't be null";

            if (string.IsNullOrWhiteSpace(model.Name))
                return $"{nameof(model.Name)} is required";

            if (model.DatabaseCode < 1)
                return $"{nameof(model.DatabaseCode)} is required";

            if (model.ServerCode < 1)
                return $"{nameof(model.ServerCode)} is required";


            return "";
        }

        #endregion

        #region Actions

        [Route("All")]
        public virtual IActionResult Index()
        {
            if (!_permissionService.Authorize())
                return AccessDeniedView();

            return View();
        }

        [Route("APIsList"), HttpPost]
        public virtual IActionResult APIsList(DataSourceRequest command,int serverCode = 0)
        {
            if (!_permissionService.Authorize())
                return AccessDeniedKendoGridJson();

            var data = _applicationService.GetAllAPIs(serverCode, command.Page, command.PageSize);
            var gridModel = new DataSourceResult
            {
                Data = data,
                Total = data.TotalCount
            };

            return Json(gridModel);
        }

        [Route("Add")]
        public virtual IActionResult Add()
        {
            if (!_permissionService.Authorize())
                return AccessDeniedView();

            var model = new APIModel();
            PrepareModel(ref model, null);
            return View(model);
        }


        [Route("Add"), HttpPost]
        public virtual IActionResult PostAdd(APIModel model)
        {
            if (!_permissionService.Authorize())
                return AccessDeniedView();

            string errorMessage = ValidateModel(model);
            if (!string.IsNullOrWhiteSpace(errorMessage))
            {
                ErrorNotification(errorMessage);
                PrepareModel(ref model, model);
                return View("Add", model);
            }


            if (ModelState.IsValid)
            {
                var entity = new API(model);
                _applicationService.AddAPI(entity);
                SuccessNotification($"Item has been added successfully");
                return RedirectToAction("Index");
            }

            return View("Add", model);
        }


        [Route("Edit/{code}")]
        public virtual IActionResult Edit(int code)
        {
            if (!_permissionService.Authorize())
                return AccessDeniedView();

            var entity = _applicationService.GetAPIByCode(code);
            if (entity == null)
            {
                ErrorNotification($"Can't find data with code ({code})");
                return RedirectToAction("Index");
            }
            var model = new APIModel(entity);
            PrepareModel(ref model, entity);
            return View(model);
        }

        [Route("Edit"), HttpPost]
        public virtual IActionResult PostEdit(APIModel model)
        {
            if (!_permissionService.Authorize())
                return AccessDeniedView();

            string errorMessage = ValidateModel(model);
            if (!string.IsNullOrWhiteSpace(errorMessage))
            {
                ErrorNotification(errorMessage);
                PrepareModel(ref model, model);
                return View("Edit", model);
            }

            if (ModelState.IsValid)
            {
                var entity = new API(model);
                _applicationService.UpdateAPI(entity);
                SuccessNotification($"Item has been updated successfully");
                return RedirectToAction("Index");
            }

            return View("Edit", model);
        }

        [Route("Delete/{code}")]
        public virtual IActionResult Delete(int code, bool isAjax = false)
        {
            if (!_permissionService.Authorize())
                return AccessDeniedView();

            var entity = _applicationService.GetAPIByCode(code);
            if (entity == null)
                return RedirectToAction("Index");

            try
            {
                //delete
                bool success = _applicationService.DeleteAPI(entity.Code);
                if (isAjax)
                    return Json(new { success = success });

                if (success)
                    SuccessNotification($"Item has been deleted successfully");
                else
                    ErrorNotification($"Delete Item faild");
                return RedirectToAction("Index");
            }
            catch (Exception exc)
            {
                if (isAjax)
                    return Json(new { success = false });
                ErrorNotification(exc.Message);
                return RedirectToAction("Index", new { id = entity.Code });
            }
        }

        #endregion

    }
}