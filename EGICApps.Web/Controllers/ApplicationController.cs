﻿using EGIC.Core.Domain.Applications;
using EGIC.Core.Models.Applications;
using EGIC.Web.Framework.Kendoui;
using EGICApps.Services.Applications;
using EGICApps.Services.Common;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;

namespace EGICApps.Web.Controllers
{
    [Route("Application")]
    public class ApplicationController : BaseController
    {

        #region Fields

        private readonly IApplicationService _applicationService;
        private readonly ICommonService _commonService;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly IConfiguration _config;

        #endregion

        #region Ctor

        public ApplicationController(IServiceProvider service,
            IApplicationService serverService,
            ICommonService commonService,
            IHostingEnvironment hostingEnvironment,
            IConfiguration config) : base(service)
        {
            _hostingEnvironment = hostingEnvironment;
            _applicationService = serverService;
            _commonService = commonService;
            _config = config;
        }

        #endregion

        #region Utilities

        private void PrepareModel(ref ApplicationModel model, Application entity)
        {
            int? selectedServer = null;
            int? selectedDatabase = null;
            int? selectedAPI = null;
            if (entity != null)
            {
                selectedServer = entity.HostServerCode;
                selectedDatabase = entity.DatabaseCode;
                selectedAPI = entity.APICode;
            }

            model.Servers = _commonService.GetAllServers(selectedServer);
            model.Databases = _commonService.GetAllDatabases(selectedDatabase);
            model.APIs = _commonService.GetAllAPIs(selectedAPI);
        }


        private string ValidateModel(ApplicationModel model)
        {
            if (model == null)
                return $"{nameof(model)} can't be null";

            if (string.IsNullOrWhiteSpace(model.Name))
                return $"{nameof(model.Name)} is required";


            return "";
        }

        #endregion

        #region Actions

        [Route("All")]
        public virtual IActionResult Index()
        {
            if (!_permissionService.Authorize())
                return AccessDeniedView();

            var model = new ApplicationListModel();
            model.HostingServers = _commonService.GetAllServers();
            model.PlatformTypes = _commonService.GetAllPlatformTypes();
            model.Responsibles = _commonService.GetAllResponsibles();

            return View(model);
        }

        [Route("ApplicationsList"), HttpPost]
        public virtual IActionResult ApplicationsList(DataSourceRequest command, int serverCode = 0,string responsible=null, string platformType=null)
        {
            if (!_permissionService.Authorize())
                return AccessDeniedKendoGridJson();

            var data = _applicationService.GetAllApplications(serverCode, responsible, platformType, command.Page, command.PageSize);
            var gridModel = new DataSourceResult
            {
                Data = data,
                Total = data.TotalCount
            };

            return Json(gridModel);
        }

        [Route("Add")]
        public virtual IActionResult Add()
        {
            if (!_permissionService.Authorize())
                return AccessDeniedView();

            var model = new ApplicationModel();
            PrepareModel(ref model, null);
            return View(model);
        }


        [Route("Add"), HttpPost]
        public virtual IActionResult PostAdd(ApplicationModel model)
        {
            if (!_permissionService.Authorize())
                return AccessDeniedView();

            string errorMessage = ValidateModel(model);
            if (!string.IsNullOrWhiteSpace(errorMessage))
            {
                ErrorNotification(errorMessage);
                PrepareModel(ref model, model);
                return View("Add", model);
            }


            if (ModelState.IsValid)
            {
                var entity = new Application(model);
                _applicationService.AddApplication(entity);
                SuccessNotification($"Item has been added successfully");
                return RedirectToAction("Index");
            }

            return View("Add", model);
        }


        [Route("Edit/{code}")]
        public virtual IActionResult Edit(int code)
        {
            if (!_permissionService.Authorize())
                return AccessDeniedView();

            var entity = _applicationService.GetApplicationByCode(code);
            if (entity == null)
            {
                ErrorNotification($"Can't find data with code ({code})");
                return RedirectToAction("Index");
            }
            var model = new ApplicationModel(entity);
            PrepareModel(ref model, entity);
            return View(model);
        }

        [Route("Edit"), HttpPost]
        public virtual IActionResult PostEdit(ApplicationModel model)
        {
            if (!_permissionService.Authorize())
                return AccessDeniedView();

            string errorMessage = ValidateModel(model);
            if (!string.IsNullOrWhiteSpace(errorMessage))
            {
                ErrorNotification(errorMessage);
                PrepareModel(ref model, model);
                return View("Edit", model);
            }

            if (ModelState.IsValid)
            {
                var entity = new Application(model);
                _applicationService.UpdateApplication(entity);
                SuccessNotification($"Item has been updated successfully");
                return RedirectToAction("Index");
            }

            return View("Edit", model);
        }

        [Route("Delete/{code}")]
        public virtual IActionResult Delete(int code, bool isAjax = false)
        {
            if (!_permissionService.Authorize())
                return AccessDeniedView();

            var entity = _applicationService.GetApplicationByCode(code);
            if (entity == null)
                return RedirectToAction("Index");

            try
            {
                //delete
                bool success = _applicationService.DeleteApplication(entity.Code);
                if (isAjax)
                    return Json(new { success = success });

                if (success)
                    SuccessNotification($"Item has been deleted successfully");
                else
                    ErrorNotification($"Delete Item faild");
                return RedirectToAction("Index");
            }
            catch (Exception exc)
            {
                if (isAjax)
                    return Json(new { success = false });
                ErrorNotification(exc.Message);
                return RedirectToAction("Index", new { id = entity.Code });
            }
        }

        #endregion

    }
}