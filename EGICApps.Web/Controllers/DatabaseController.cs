﻿using EGIC.Core.Domain.Databases;
using EGIC.Core.Models.Databases;
using EGIC.Web.Framework.Kendoui;
using EGICApps.Services.Common;
using EGICApps.Services.Databases;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;

namespace EGICApps.Web.Controllers
{
    [Route("Database")]
    public class DatabaseController : BaseController
    {

        #region Fields

        private readonly IDatabaseService _databaseService;
        private readonly ICommonService _commonService;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly IConfiguration _config;

        #endregion

        #region Ctor

        public DatabaseController(IServiceProvider service,
            IDatabaseService serverService,
            ICommonService commonService,
            IHostingEnvironment hostingEnvironment,
            IConfiguration config) : base(service)
        {
            _hostingEnvironment = hostingEnvironment;
            _databaseService = serverService;
            _commonService = commonService;
            _config = config;
        }

        #endregion

        #region Utilities

        private void PrepareModel(ref DatabaseModel model, Database entity)
        {
            int? selectedServer = null;
            string selectedEnvironment = null;
            if (entity != null)
            {
                selectedServer = entity.ServerCode;
                selectedEnvironment = entity.Environment;
            }

            model.Servers = _commonService.GetAllServers(selectedServer);
            model.Environments = _commonService.GetAllEnvironments(selectedEnvironment);
        }


        private string ValidateModel(DatabaseModel model)
        {
            if (model == null)
                return $"{nameof(model)} can't be null";

            if (string.IsNullOrWhiteSpace(model.ServiceName))
                return $"{nameof(model.ServiceName)} is required";

            if (string.IsNullOrWhiteSpace(model.UserSchema))
                return $"{nameof(model.UserSchema)} is required";

            if (string.IsNullOrWhiteSpace(model.Password))
                return $"{nameof(model.Password)} is required";


            return "";
        }

        #endregion

        #region Actions

        [Route("All")]
        public virtual IActionResult Index()
        {
            if (!_permissionService.Authorize())
                return AccessDeniedView();

            return View();
        }

        [Route("DatabasesList"), HttpPost]
        public virtual IActionResult DatabasesList(DataSourceRequest command, int serverCode = 0)
        {
            if (!_permissionService.Authorize())
                return AccessDeniedKendoGridJson();

            var data = _databaseService.GetAllDatabases(serverCode, command.Page, command.PageSize);
            var gridModel = new DataSourceResult
            {
                Data = data,
                Total = data.TotalCount
            };

            return Json(gridModel);
        }

        [Route("Add")]
        public virtual IActionResult Add()
        {
            if (!_permissionService.Authorize())
                return AccessDeniedView();

            var model = new DatabaseModel();
            PrepareModel(ref model, null);
            return View(model);
        }


        [Route("Add"), HttpPost]
        public virtual IActionResult PostAdd(DatabaseModel model)
        {
            if (!_permissionService.Authorize())
                return AccessDeniedView();

            string errorMessage = ValidateModel(model);
            if (!string.IsNullOrWhiteSpace(errorMessage))
            {
                ErrorNotification(errorMessage);
                PrepareModel(ref model, model);
                return View("Add", model);
            }


            if (ModelState.IsValid)
            {
                var entity = new Database(model);
                _databaseService.AddDatabase(entity);
                SuccessNotification($"Item has been added successfully");
                return RedirectToAction("Index");
            }

            return View("Add", model);
        }


        [Route("Edit/{code}")]
        public virtual IActionResult Edit(int code)
        {
            if (!_permissionService.Authorize())
                return AccessDeniedView();

            var entity = _databaseService.GetDatabaseByCode(code);
            if (entity == null)
            {
                ErrorNotification($"Can't find data with code ({code})");
                return RedirectToAction("Index");
            }
            var model = new DatabaseModel(entity);
            PrepareModel(ref model, entity);
            return View(model);
        }

        [Route("Edit"), HttpPost]
        public virtual IActionResult PostEdit(DatabaseModel model)
        {
            if (!_permissionService.Authorize())
                return AccessDeniedView();

            string errorMessage = ValidateModel(model);
            if (!string.IsNullOrWhiteSpace(errorMessage))
            {
                ErrorNotification(errorMessage);
                PrepareModel(ref model, model);
                return View("Edit", model);
            }

            if (ModelState.IsValid)
            {
                var entity = new Database(model);
                _databaseService.UpdateDatabase(entity);
                SuccessNotification($"Item has been updated successfully");
                return RedirectToAction("Index");
            }

            return View("Edit", model);
        }

        [Route("Delete/{code}")]
        public virtual IActionResult Delete(int code, bool isAjax = false)
        {
            if (!_permissionService.Authorize())
                return AccessDeniedView();

            var entity = _databaseService.GetDatabaseByCode(code);
            if (entity == null)
                return RedirectToAction("Index");

            try
            {
                //delete
                bool success = _databaseService.DeleteDatabase(entity.Code);
                if (isAjax)
                    return Json(new { success = success });

                if (success)
                    SuccessNotification($"Item has been deleted successfully");
                else
                    ErrorNotification($"Delete Item faild");
                return RedirectToAction("Index");
            }
            catch (Exception exc)
            {
                if (isAjax)
                    return Json(new { success = false });
                ErrorNotification(exc.Message);
                return RedirectToAction("Index", new { id = entity.Code });
            }
        }

        #endregion

    }
}