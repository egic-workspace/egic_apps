﻿using EGIC.Core.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Diagnostics;

namespace EGICApps.Web.Controllers
{
    public class HomeController : BaseController
    {
        #region Fields

        #endregion

        #region CTOR

        public HomeController(IServiceProvider service) : base(service)
        {
        }

        #endregion

        public IActionResult Index()
        {
            if (!_permissionService.Authorize())
                return AccessDeniedView();

            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}