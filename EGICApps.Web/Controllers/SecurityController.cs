﻿using EGIC.Core.Domain.Users;
using EGIC.Core.Models.Security;
using EGIC.Core.Models.Users;
using Microsoft.AspNetCore.Mvc;
using EGICApps.Services.Authentication;
using System;

namespace EGICApps.Web.Controllers
{
    public class SecurityController : BaseController
    {
        #region Fields

        private readonly IAuthenticationService _authenticationService;

        #endregion

        #region CTOR

        public SecurityController(
            IServiceProvider service,
            IAuthenticationService authenticationService) : base(service)
        {
            _authenticationService = authenticationService;
        }

        #endregion


        #region Actions

        #region Login/Logout

        [Route("/login")]
        public IActionResult Login()
        {
            var model = new LoginModel();
            return View(model);
        }

        [HttpPost]
        [Route("/login")]
        public virtual IActionResult Login(LoginModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                model.Username = model.Username.Trim().ToUpper();
                var loginResult = _authenticationService.ValidateUser(model.Username, model.Password, out User user);
                switch (loginResult)
                {
                    case UserLoginResults.Successful:
                        {


                            //sign in new user
                            _authenticationService.SignIn(user);

                            if (string.IsNullOrEmpty(returnUrl) || !Url.IsLocalUrl(returnUrl))
                                return RedirectToAction("Index", "Home");

                            return Redirect(returnUrl);
                        }
                    case UserLoginResults.UserNotExist:
                        ModelState.AddModelError("", "User not existed");
                        break;
                    case UserLoginResults.Deleted:
                        ModelState.AddModelError("", "User has been deactivated");
                        break;
                    case UserLoginResults.WrongPassword:
                    default:
                        ModelState.AddModelError("", "Wrong password, please make sure your password and try again.");//
                        break;
                }
            }

            return View(model);
        }

        [Route("/logout")]
        public virtual IActionResult Logout()
        {
            //standard logout 
            _authenticationService.SignOut();

            return RedirectToAction("Login");
        }

        #endregion

        public virtual IActionResult AccessDenied(string pageUrl)
        {
            var currentUser = _workContext.CurrentUser;
            return View();
        }

        #endregion
    }
}