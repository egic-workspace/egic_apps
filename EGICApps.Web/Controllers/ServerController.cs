﻿using EGIC.Core.Domain.Servers;
using EGIC.Core.Models.Servers;
using EGIC.Web.Framework.Kendoui;
using EGICApps.Services.Common;
using EGICApps.Services.Servers;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;

namespace EGICApps.Web.Controllers
{
    [Route("Server")]
    public class ServerController : BaseController
    {

        #region Fields

        private readonly IServerService _serverService;
        private readonly ICommonService _commonService;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly IConfiguration _config;

        #endregion

        #region Ctor

        public ServerController(IServiceProvider service,
            IServerService serverService,
            ICommonService commonService,
            IHostingEnvironment hostingEnvironment,
            IConfiguration config) : base(service)
        {
            _hostingEnvironment = hostingEnvironment;
            _serverService = serverService;
            _commonService = commonService;
            _config = config;
        }

        #endregion

        #region Utilities
        /*
        private void PrepareModel(ref ServerModel model, object p)
        {
            throw new NotImplementedException();
        }
        */

        private string ValidateModel(ServerModel model)
        {
            if (model == null)
                return "Model can't be null";
            
            if (string.IsNullOrWhiteSpace(model.Name))
                return "Name is required";
            
            if (string.IsNullOrWhiteSpace(model.InternalIP))
                return "InternalIP is required";
            
            //if (string.IsNullOrWhiteSpace(model.ExternalIP))
            //    return "ExternalIP is required";
            
            
            return "";
        }
        #endregion

        #region Actions

        [Route("All")]
        public virtual IActionResult Index()
        {
            if (!_permissionService.Authorize())
                return AccessDeniedView();

            var model = new ServerListModel();
            model.ServerLocations = _commonService.GetAllLocations();
            return View(model);
        }

        [Route("ServersList"), HttpPost]
        public virtual IActionResult ServersList(DataSourceRequest command, ServerListModel model)
        {
            if (!_permissionService.Authorize())
                return AccessDeniedKendoGridJson();

            var data = _serverService.GetAllServers(model.SearchServerName, model.SearchServerLocation, model.SearchServerInternalIP, model.SearchServerExternalIP, command.Page, command.PageSize);
            var gridModel = new DataSourceResult
            {
                Data = data,
                Total = data.TotalCount
            };

            return Json(gridModel);
        }

        [Route("Add")]
        public virtual IActionResult Add()
        {
            if (!_permissionService.Authorize())
                return AccessDeniedView();

            var model = new ServerModel();
            //PrepareModel(ref model, null);
            return View(model);
        }


        [Route("Add"), HttpPost]
        public virtual IActionResult PostAdd(ServerModel model)
        {
            if (!_permissionService.Authorize())
                return AccessDeniedView();

            string errorMessage = ValidateModel(model);
            if (!string.IsNullOrWhiteSpace(errorMessage))
            {
                ErrorNotification(errorMessage);
                //PrepareModel(ref model, null);
                return View("Add", model);
            }


            if (ModelState.IsValid)
            {
                var entity = new Server(model);
                _serverService.AddServer(entity);
                SuccessNotification($"Item has been added successfully");
                return RedirectToAction("Index");
            }

            return View("Add", model);
        }


        [Route("Edit/{code}")]
        public virtual IActionResult Edit(int code)
        {
            if (!_permissionService.Authorize())
                return AccessDeniedView();

            var entity = _serverService.GetServerByCode(code);
            if (entity == null)
            {
                ErrorNotification($"Can't find data with code ({code})");
                return RedirectToAction("Index");
            }
            var model = new ServerModel(entity);
            return View(model);
        }

        [Route("Edit"), HttpPost]
        public virtual IActionResult PostEdit(ServerModel model)
        {
            if (!_permissionService.Authorize())
                return AccessDeniedView();

            string errorMessage = ValidateModel(model);
            if (!string.IsNullOrWhiteSpace(errorMessage))
            {
                ErrorNotification(errorMessage);
                return View("Edit", model);
            }

            if (ModelState.IsValid)
            {
                var entity = new Server(model);
                _serverService.UpdateServer(entity);
                SuccessNotification($"Item has been updated successfully");
                return RedirectToAction("Index");
            }

            return View("Edit", model);
        }

        [Route("Delete/{code}")]
        public virtual IActionResult Delete(int code, bool isAjax = false)
        {
            if (!_permissionService.Authorize())
                return AccessDeniedView();

            var entity = _serverService.GetServerByCode(code);
            if (entity == null)
                return RedirectToAction("Index");

            try
            {
                //delete
                bool success = _serverService.DeleteServer(entity.Code);
                if (isAjax)
                    return Json(new { success = success });

                if (success)
                    SuccessNotification($"Item has been deleted successfully");
                else
                    ErrorNotification($"Delete Item faild");
                return RedirectToAction("Index");
            }
            catch (Exception exc)
            {
                if (isAjax)
                    return Json(new { success = false });
                ErrorNotification(exc.Message);
                return RedirectToAction("Index", new { id = entity.Code });
            }
        }

        #endregion

    }
}