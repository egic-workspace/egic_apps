﻿namespace EGICApps.Web.Models.EmailTemplates
{
    public class EmailTemplateModel
    {
        public string SendToFullName { get; set; }
        public string SendFromFullName { get; set; }
        public string MessageBody { get; set; }
        public string Url { get; set; }
    }
}
