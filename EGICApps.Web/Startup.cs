﻿using EGIC.Data.DAL;
using EGICApps.Services;
using EGICApps.Services.Authentication;
using EGICApps.Services.Common;
using EGICApps.Services.Servers;
using EGICApps.Services.Security;
using EGICApps.Services.Users;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Globalization;
using EGICApps.Services.Databases;
using EGICApps.Services.APIs;
using EGICApps.Services.Applications;

namespace EGIC.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddSession(options =>
            {
                // Set a short timeout for easy testing.
                options.IdleTimeout = TimeSpan.FromHours(1);
                options.Cookie.HttpOnly = true;
                // Make the session cookie essential
                options.Cookie.IsEssential = true;
            });

            services.AddMvc();

            //set default authentication schemes
            var authenticationBuilder = services.AddAuthentication(options =>
            {
                options.DefaultChallengeScheme = EGICCookieAuthenticationDefaults.AuthenticationScheme;
                options.DefaultSignInScheme = EGICCookieAuthenticationDefaults.ExternalAuthenticationScheme;
            });

            //add main cookie authentication
            authenticationBuilder.AddCookie(EGICCookieAuthenticationDefaults.AuthenticationScheme, options =>
            {
                options.Cookie.Name = EGICCookieAuthenticationDefaults.CookiePrefix + EGICCookieAuthenticationDefaults.AuthenticationScheme;
                options.Cookie.HttpOnly = true;
                options.LoginPath = EGICCookieAuthenticationDefaults.LoginPath;
                options.AccessDeniedPath = EGICCookieAuthenticationDefaults.AccessDeniedPath;
            });

            //add external authentication
            authenticationBuilder.AddCookie(EGICCookieAuthenticationDefaults.ExternalAuthenticationScheme, options =>
            {
                options.Cookie.Name = EGICCookieAuthenticationDefaults.CookiePrefix + EGICCookieAuthenticationDefaults.ExternalAuthenticationScheme;
                options.Cookie.HttpOnly = true;
                options.LoginPath = EGICCookieAuthenticationDefaults.LoginPath;
                options.AccessDeniedPath = EGICCookieAuthenticationDefaults.AccessDeniedPath;
            });

            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddSingleton<IConfiguration>(Configuration);
            services.AddSingleton<IOracleAcess, OracleAcess>();
            services.AddScoped<IWorkContext, WebWorkContext>();
            services.AddTransient<IUserService, UserService>(); 
            services.AddTransient<IPermissionService, PermissionService>();
            services.AddTransient<IAuthenticationService, CookieAuthenticationService>();
            services.AddTransient<IServerService, ServerService>(); 
            services.AddTransient<IDatabaseService, DatabaseService>(); 
            services.AddTransient<IAPIService, APIService>(); 
            services.AddTransient<IApplicationService, ApplicationService>(); 
            services.AddTransient<ICommonService, CommonService>(); 
            services.Configure<IISOptions>(options =>
            {

            });

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            var cultureInfo = new CultureInfo("en-US");
            cultureInfo.DateTimeFormat.ShortDatePattern = "dd/MM/yyyy";

            CultureInfo.DefaultThreadCurrentCulture = cultureInfo;
            CultureInfo.DefaultThreadCurrentUICulture = cultureInfo;

            if (env.IsDevelopment())
            {
                app.UseBrowserLink();
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseMiddleware<AuthenticationMiddleware>();
            app.UseStaticFiles();
            app.UseSession();
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
